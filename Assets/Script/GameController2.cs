﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Text;
using System;

public class GameController2 : MonoBehaviour
{
	public AudioSource nextAudioSource;
	public AudioSource finishAudioSource;
	public AudioSource StartAudioSource;
	public AudioSource StoptAudioSource;
	public AudioSource TitleAudioSource;

	public Text TitleText;

	public GameObject Roulette;
	public Text RouletteText;

	public GameObject InputPersonObject;
	public InputField InputPerson;

	public Image IsStopImage;

	public GameObject startButton;
	public GameObject stopButton;
	public GameObject resetButton;

	public GameObject returnButton;

	public double TimeGap;
	private double TimeCount;
	private double time;

	private int colorCountStay = 0;
	private int colorCountDown = 0;

	public bool isPlaying = false;
	public bool isStopping = false;
	public bool isPausingForStartVoice = false;
	public bool isPausingForStopVoice = false;

	private int resultInt = 1;
	private string resultString;
	private int personsInt;
	private string personsString;

	private int previousResult;

	// Start is called before the first frame update
	void Start()
    {
		stopButton.SetActive(false);
		resetButton.SetActive(false);

		returnButton.SetActive(true);

		TitleText.text = "Number Roulette";

		RouletteText.text = "";

		InputPersonObject.SetActive(true);
		InputPerson.ActivateInputField();
	}

	// Update is called once per frame
	void Update()
    {
		if (isPausingForStartVoice)
		{
			time = time + Time.deltaTime;
			if (time >= 0.9f)
			{
				time = 0;
				isPausingForStartVoice = false;
			}
		}
		else if (isPausingForStopVoice) {
			time = time + Time.deltaTime;
			if (time >= 0.5f) {
				time = 0;
				isPausingForStopVoice = false;
			}
		}
		else
		{
			if (isPlaying)
			{
				if (isStopping)
				{
					StopMethod();
				}
				else
				{
					StartMethod();
				}
			}
		}
    }

	public void OnClickStartButton()
	{
		isPlaying = true;
		isPausingForStartVoice = true;
		time = 0.0f;

		TimeCount = TimeGap;

		TitleText.text = "";

		startButton.SetActive(false);
		stopButton.SetActive(true);

		StartAudioSource.Play();

		InputPersonObject.SetActive(false);
	}

	public void OnClickStopButton()
	{
		isStopping = true;
		isPausingForStopVoice = true;
		time = 0.0f;

		StoptAudioSource.Play();

		stopButton.SetActive(false);

		TimeCount = 0.5;
	}

	public void OnClickResetButton()
	{
		SceneManager.LoadScene(0);
	}

	void StartMethod()
	{
		TimeCount -= Time.deltaTime;

		if (TimeCount <= 0)
		{
			personsString = InputPerson.text;
			personsInt = Int32.Parse(personsString);

			RandomRoulette();

			//RouletteText.text = result.ToString();

			//result = (result + 1) % personsInt;

			//if (result == 0) {
			//	result = personsInt;
			//}

			nextAudioSource.Play();

			colorCountStay = colorCountStay + 1;
			TimeCount = TimeGap;
		}
	}

	void StopMethod()
	{
		TimeCount -= Time.deltaTime;

		if (TimeCount <= 0)
		{
			RandomRoulette();

			//RouletteText.text = result.ToString();

			//result = (result + 1) % personsInt;

			//if (result == 0)
			//{
			//	result = personsInt;
			//}

			colorCountStay = colorCountStay + 1;
			colorCountDown = colorCountDown + 1;
			TimeCount = colorCountDown * 0.1 + TimeGap;

			nextAudioSource.Play();

		}

		if (TimeCount > 1)
		{
			isStopping = false;
			isPlaying = false;

			resetButton.SetActive(true);

			finishAudioSource.Play();
		}
	}

	void RandomRoulette() {
		System.Random rnd = new System.Random();

		resultInt = rnd.Next(1, personsInt + 1);

		if (previousResult == resultInt) {
			resultInt++;

			if (resultInt > personsInt) {
				resultInt = resultInt % personsInt;
			}
		}

		previousResult = resultInt;
		RouletteText.text = resultInt.ToString();
	}
}

