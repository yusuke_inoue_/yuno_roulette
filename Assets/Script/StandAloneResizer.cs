﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandAloneResizer : MonoBehaviour
{
	public int width = 500;
	public int height = 667;

	public bool fullscreen = false;

	public int preferredRefreshRate = 60;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_STANDALONE_WIN
		Screen.SetResolution(width, height, fullscreen, preferredRefreshRate);
#endif
	}
}
