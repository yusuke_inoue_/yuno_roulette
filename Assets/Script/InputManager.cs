﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Text;
using System;

public class InputManager : MonoBehaviour
{
	public InputField InputPersons;

	public string PersonsString;

	// Start is called before the first frame update
	void Start()
    {
		InitInputField();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void DisplayNumber()
	{
		PersonsString = InputPersons.text;
	}

	void InitInputField()
	{
		// 値をリセット
		InputPersons.text = "";

		// フォーカス
		InputPersons.ActivateInputField();
	}
}
