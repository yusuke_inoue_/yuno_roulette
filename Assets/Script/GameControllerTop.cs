﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Text;
using System;

public class GameControllerTop : MonoBehaviour
{
	public GameObject CanvasTop;
	public GameObject Canvas;

	public GameObject DiscountRoulette;
	public GameObject NumberRoulette;

	public Text NoteText;
	public Text TitleText;

	public GameObject SelectDiscountButton;
	public GameObject SelectNumberButton;

	// Start is called before the first frame update
	void Start()
    {
		CanvasTop.SetActive(true);
		Canvas.SetActive(false);

		DiscountRoulette.SetActive(false);
		NumberRoulette.SetActive(false);

		Canvas.GetComponent<GameController>().enabled = false;
		Canvas.GetComponent<GameController2>().enabled = false;
	}

	// Update is called once per frame
	void Update()
    {
        
    }

	public void OnClickSelectDiscountButton()
	{
		NoteText.text = "";
		TitleText.text = "";

		CanvasTop.SetActive(false);
		Canvas.SetActive(true);

		DiscountRoulette.SetActive(true);

		Canvas.GetComponent<GameController>().enabled = true;

	}

	public void OnClickSelectNumberButton()
	{
		NoteText.text = "";
		TitleText.text = "";

		CanvasTop.SetActive(false);
		Canvas.SetActive(true);

		NumberRoulette.SetActive(true);

		Canvas.GetComponent<GameController2>().enabled = true;
	}
}
